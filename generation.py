import itertools


from typing import Optional, Dict, Union
from nltk import sent_tokenize
import random

import spacy
import gensim
from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors

#from distractor import generate_distractors


import torch
from transformers import(
    AutoModelForSeq2SeqLM, 
    AutoTokenizer,
    PreTrainedModel,
    PreTrainedTokenizer,
)

#from gensim.scripts.glove2word2vec import glove2word2vec
#sp = spacy.load('en_core_web_sm')



class QGPipeline:
    def __init__(self):
        self.model = AutoModelForSeq2SeqLM.from_pretrained('t5-qg')
        self.tokenizer = AutoTokenizer.from_pretrained('t5-qg')

        self.ans_model = AutoModelForSeq2SeqLM.from_pretrained("t5-qa")
        self.ans_tokenizer = AutoTokenizer.from_pretrained("t5-qa")

        self.device = "cuda" if torch.cuda.is_available() else "cpu"
        self.model.to(self.device)

        self.ans_model.to(self.device)

    def __call__(self, inputs: str):
        inputs = " ".join(inputs.split())
        sents, answers = self._extract_answers(inputs)
        flat_answers = list(itertools.chain(*answers))
        
        if len(flat_answers) == 0:
          return []

        qg_examples = self._prepare_inputs_for_qg_from_answers_hl(sents, answers)
        qg_inputs = [example['source_text'] for example in qg_examples]
        questions = self._generate_questions(qg_inputs)
        output = [{'answer': example['answer'], 'question': que} for example, que in zip(qg_examples, questions)]
        return output
    
    def _generate_questions(self, inputs):
        inputs = self._tokenize(inputs, padding=True, truncation=True)        
        outs = self.model.generate(
            input_ids=inputs['input_ids'].to(self.device), 
            attention_mask=inputs['attention_mask'].to(self.device), 
            max_length=32,
            num_beams=4,
        )
        questions = [self.tokenizer.decode(ids, skip_special_tokens=True) for ids in outs]
        return questions
    
    def _extract_answers(self, context):
        sents, inputs = self._prepare_inputs_for_ans_extraction(context)
        inputs = self._tokenize(inputs, padding=True, truncation=True)

        outs = self.ans_model.generate(
            input_ids=inputs['input_ids'].to(self.device), 
            attention_mask=inputs['attention_mask'].to(self.device), 
            max_length=32,
        )
        dec = [self.ans_tokenizer.decode(ids, skip_special_tokens=False) for ids in outs]
        answers = [item.split('<sep>') for item in dec]
        answers = [i[:-1] for i in answers]        
        return sents, answers
    
    def _tokenize(self,
        inputs,
        padding=True,
        truncation=True,
        add_special_tokens=True,
        max_length=512
    ):
        inputs = self.tokenizer.batch_encode_plus(
            inputs, 
            max_length=max_length,
            add_special_tokens=add_special_tokens,
            truncation=truncation,
            padding="max_length" if padding else False,
            pad_to_max_length=padding,
            return_tensors="pt"
        )
        return inputs
    
    def _prepare_inputs_for_ans_extraction(self, text):
        sents = sent_tokenize(text)

        inputs = []
        for i in range(len(sents)):
            source_text = "extract answers:"
            for j, sent in enumerate(sents):
                if i == j:
                    sent = "<hl> %s <hl>" % sent
                source_text = "%s %s" % (source_text, sent)
                source_text = source_text.strip()
            source_text = source_text + " </s>"

            inputs.append(source_text)

        return sents, inputs
    
    def _prepare_inputs_for_qg_from_answers_hl(self, sents, answers):
        inputs = []
        for i, answer in enumerate(answers):
            if len(answer) == 0: continue
            sent = sents[i]
            for answer_text in answer:
                sents_copy = sents[:]
                
                answer_text = answer_text.strip()
                try:
                    ans_start_idx = sent.index(answer_text)
                except:
                    continue
                sent = f"{sent[:ans_start_idx]} <hl> {answer_text} <hl> {sent[ans_start_idx + len(answer_text): ]}"
                sents_copy[i] = sent
                
                source_text = " ".join(sents_copy)
                source_text = f"generate question: {source_text}" 
                source_text = source_text + " </s>"
                inputs.append({"answer": answer_text, "source_text": source_text})
        return inputs

#for using multiple choices uncomment the areas below and lines 13, 24, 25

"""def choice_creator(answer):
    result = []
    answers = answer.split()
    rand_num = random.randint(0, len(answers)-1)
    stop=0
    
    while( answers[rand_num] in sp.Defaults.stop_words):
        rand_num = random.randint(0, len(answers)-1)
        stop+=1
        if stop == len(answers):
            rand_num = len(answers)-1
            break
    wrongs = generate_distractors((answer.split())[rand_num],3)
    for wrong in wrongs:
        for i,word in enumerate(answers):
            if i == rand_num:
                continue    
            wrong.insert(i,word)
    for choice in wrongs:
        result.append(" ".join(choice))
    return result"""



qg =  QGPipeline()
while(1):
    context = input("\n context : ")
    qa_pairs = qg(context)
    for pair in qa_pairs:
        print("\n","Question: ", pair['question'])
        print("\n", "Answer: ", pair['answer']) #for not to use extra choice system
        """answers = choice_creator(pair['answer'])
        answers.append( (pair['answer'] + " (True)") )  
        random.shuffle(answers)
        for answer in answers:
            print(answer, "\n")"""


