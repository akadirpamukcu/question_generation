import os
import logging
from dataclasses import dataclass, field
from typing import Dict, List, Optional

import torch
import nlp
from transformers import T5Tokenizer, BartTokenizer, HfArgumentParser

logger = logging.getLogger(__name__)

@dataclass
class DataTrainingArguments:
    task: str 

    train_file_name: str

    valid_file_name: str

    valid_for_qg_only: bool = field(default=False)

class DataProcessor:
    def __init__(self, tokenizer):
        self.tokenizer = tokenizer
        self.max_source_length = 512
        self.max_target_length = 32
        self.model_type = "t5" 

    def process(self, dataset):
        dataset = dataset.map(self._add_end_of_sentence)
        dataset = dataset.map(self._add_special_tokens)
        dataset = dataset.map(self._convert_to_features, batched=True)       
        return dataset

    # indicate end of sentences
    def _add_end_of_sentence(self, example):
        example['source_text'] = example['source_text'] + " </s>"
        example['target_text'] = example['target_text'] + " </s>"
        return example

    # replace token markers with tokens
    def _add_special_tokens(self, example):
        example['source_text'] = example['source_text'].replace("{hl_token}", "<hl>")    
        example['target_text'] = example['target_text'].replace("{sep_token}", "<sep>")
        return example
  
    # tokenize the examples
    def _convert_to_features(self, example_batch):
        source_encoding = self.tokenizer.batch_encode_plus(
            example_batch['source_text'],
            max_length=self.max_source_length,
            padding='max_length',
            pad_to_max_length=True,
            truncation=True, 
        )
        target_encoding = self.tokenizer.batch_encode_plus(
            example_batch['target_text'],
            max_length=self.max_target_length,
            padding='max_length',
            pad_to_max_length=True,
            truncation=True, 
        )
        encodings = {
            'source_ids': source_encoding['input_ids'], 
            'target_ids': target_encoding['input_ids'],
            'attention_mask': source_encoding['attention_mask'],
        }

        return encodings

def main():
    parser = HfArgumentParser((DataTrainingArguments))

    data_args = parser.parse_args_into_dataclasses()[0]

    tokenizer = T5Tokenizer.from_pretrained("t5-base")
    tokenizer.add_tokens(['<sep>', '<hl>'])
    
    train_dataset = nlp.load_dataset("data/squad_load", name='highlight_qg_format', split=nlp.Split.TRAIN)
    valid_dataset = nlp.load_dataset("data/squad_load", name='highlight_qg_format', split=nlp.Split.VALIDATION)

    processor = DataProcessor(tokenizer)

    def filter_qg(ex):
        return ex['task'] == 'qg'
    def filter_qa(ex):
        return ex['task'] != "dummy"
    
    if data_args.task == "qg":
        train_dataset = train_dataset.filter(filter_qg)
        valid_dataset = valid_dataset.filter(filter_qg)
    elif data_args.task == "qa":
        train_dataset = train_dataset.filter(filter_qa)
        valid_dataset = valid_dataset.filter(filter_qg)

    train_dataset = processor.process(train_dataset)
    valid_dataset = processor.process(valid_dataset)

    columns = ["source_ids", "target_ids", "attention_mask"]
    train_dataset.set_format(type='torch', columns=columns)
    valid_dataset.set_format(type='torch', columns=columns)

    train_path = os.path.join("data", data_args.train_file_name)
    valid_path = os.path.join("data", data_args.valid_file_name)
    
    torch.save(train_dataset, train_path)
    logger.info(f"saved train dataset at {train_path}")
   
    torch.save(valid_dataset, valid_path)
    logger.info(f"saved validation dataset at {valid_path}")
    
    tokenizer_path = "t5_qg_tokenizer"
    if not os.path.exists(tokenizer_path):
        os.mkdir(tokenizer_path)
    tokenizer.save_pretrained(tokenizer_path)
    logger.info(f"saved tokenizer at {tokenizer_path}")

if __name__ == "__main__":
    main()

