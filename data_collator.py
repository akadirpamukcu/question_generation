from typing import Dict, List, Optional
import torch

def trim_batch(input_ids, pad_token_id, attention_mask=None):
    """Remove columns that are populated exclusively by pad_token_id"""
    keep_column_mask = input_ids.ne(pad_token_id).any(dim=0)
    if attention_mask is None:
        return input_ids[:, keep_column_mask]
    else:
        return (input_ids[:, keep_column_mask], attention_mask[:, keep_column_mask])

class T2TDataCollator():
    def __init__(self, tokenizer, mode='training'):
        self.tokenizer = tokenizer
        self.model_type = "t5"
        self.mode = mode

    def __call__(self, batch: List) -> Dict[str, torch.Tensor]:
        input_ids = torch.stack([example['source_ids'] for example in batch])
        target_ids = torch.stack([example['target_ids'] for example in batch])
        attention_mask = torch.stack([example['attention_mask'] for example in batch])

        pad_token_id = self.tokenizer.pad_token_id
        
        input_ids, attention_mask = trim_batch(input_ids, pad_token_id, attention_mask=attention_mask)
        target_ids = trim_batch(target_ids, pad_token_id)
    
        lm_labels = target_ids.clone()
        decoder_input_ids = self._shift_right_t5(lm_labels)
        lm_labels[lm_labels[:, :] == pad_token_id] = -100

        params =  {
            "input_ids": input_ids, 
            "attention_mask": attention_mask,
            "labels": lm_labels,
            "decoder_input_ids": decoder_input_ids
        }
        
        return params
    
    def _shift_right_t5(self, input_ids):
        decoder_start_token_id = self.tokenizer.pad_token_id
        pad_token_id = self.tokenizer.pad_token_id

        assert (decoder_start_token_id is not None)
        shifted_input_ids = input_ids.new_zeros(input_ids.shape)
        shifted_input_ids[..., 1:] = input_ids[..., :-1].clone()
        shifted_input_ids[..., 0] = decoder_start_token_id

        assert pad_token_id is not None
        shifted_input_ids.masked_fill_(shifted_input_ids == -100, pad_token_id)

        assert torch.all(shifted_input_ids >= 0).item()

        return shifted_input_ids